# cgtbcbe - congatec Boot Container Build  Environment

cgtbcbe simplifies building default type boot containers for
the **i.MX8X** and **i.MX8MP** based congatec product portfolio.


## Requirements

Please meet the following requirements before cloning the *cgtbcbe* repository.

### Development System

cgtbcbe is compatible to most linux-based development systems (x86_64) as long
as `bash`, `git/git-lfs` and standard development tools like `make` are
installed.

### git and git-lfs

First, install *git* via your linux distributor's package system.
Afterwards install [git-lfs](https://git-lfs.github.com) **before checking out other branches!**
Otherwise obtaining the precompiled binaries (e.g. SCFW or SECO-FW) will fail without error message. 


## How to Setup cgtbcbe

There is no need to setup cgtbcbe - just create a local clone from the
congatec git server as described below.


## How to Obtain cgtbcbe - Including Precompiled Binaries

Please use one of the following commands to create a local clone of the
cgtbcbe git repository, depending on the used product-/SOC-family.

### i.MX8X Based Product Family: conga-SMX8-X (SX8X)

Please use the following command to obtain the required scripts
and binaries to build a bootcontainer for i.MX8X based congatec products:
```
git clone -b imx8x https://git.congatec.com/arm-nxp/imx8-family/misc/cgtbcbe.git cgtbcbe-imx8x
```

### i.MX8MP Based Product Family: conga-QMX8-Plus (QX8P) and conga-SMX8-Plus (SX8P)

Please use the following command to obtain the required scripts
and binaries to build a bootcontainer for i.MX8MP based congatec products:
```
git clone -b imx8mp https://git.congatec.com/arm-nxp/imx8-family/misc/cgtbcbe.git cgtbcbe-imx8mp
```


## How to Use cgtbcbe

The central component is a script called `mkbcbe.sh`.
It's important to know, `mkbcbe.sh` changes its behaviour depending on how it
is called:

**Option 1:** Calling `mkbcbe.sh` like an ordinary bash script, prints a help
describing how to **create** a bootcontainer build environment:
```
$ cd cgtbcbe
$ ./mkbcbe.sh

usage:
  ./mkbcbe.sh -b <BUILD_DIR> -c <CONFIG_TO_USE> [-u UBOOT_BINARY]

  required switches:
    -b   build directory (environment) to be created by mkbcbe.sh
    -c   config file to use
  optional switches, (re)defining binaries to be copied to the build env:
    -u   u-boot binary
    -s   redefine SCFW binary
    -m   m40 firmware binary
    -n   m41 firmware binary
    -t   don't copy all required files to build env; just create links
```

**Option 2:** Sourcing `mkbcbe.sh` prints a help describing how to **activate**
a bootcontainer build environment:
```
$ cd cgtbcbe
$ source ./mkbcbe.sh

usage:
  source ./mkbcbe.sh <BUILD_DIR_TO_ACTIVATE>
```

**INFO**: 
Always call/source `mkbcbe.sh` via the link at the top-level-directory. Calling
it from or via the `script/` subdirectory will fail.

### How to Setup a BootContainer Build Environment

In order to build a bootcontainer, a bootcontainer build environment has to be
set up first. This is done by **calling** `mkbcbe.sh` like an ordinary script.

The caller has to specify the name of the build-directory to create (`-b`) and
the config file (`-c`) defining the module/SW specifics:
```
$ cd cgtbcbe
$ ./mkbcbe.sh -b BUILD_SX8X_2GB -c mx8x/config/cgtsx8x-Y1-2GB_BSP-4.14.98_2.0.0_SCFW-1.2.1__rel_cgtsx8x_20191029.inc.sh

PASS: build container build environment created at BUILD_SX8X_2GB
In order to activate it, run:
source ./mkbcbe.sh BUILD_SX8X_2GB
```

Let's take a look at the created bootcontainer build environment:
```
$ cd cgtbcbe
$ ls -la BUILD_SX8X_2GB/
.beconfig.inc.sh
bl31.413e93e.bin*
bl31.bin -> bl31.413e93e.bin*
fspi_header -> fspi_header.dd023400
fspi_header.dd023400
fspi_packer.dd023400.sh*
fspi_packer.sh -> fspi_packer.dd023400.sh*
Makefile*
mkimage_imx8 -> mkimage_imx8.dd023400*
mkimage_imx8.dd023400*
mx8qx-ahab-container-2.3.1.img
mx8qx-ahab-container.img -> mx8qx-ahab-container-2.3.1.img
pad_image.dd023400.sh*
pad_image.sh -> pad_image.dd023400.sh*
scfw_tcm.2GB.ffc989aa.422ae6a.bin*
scfw_tcm.bin -> scfw_tcm.2GB.ffc989aa.422ae6a.bin*
```

Creating a bootcontainer build environment roughly means:
- create a separated build-directory
- copy all required files to the build-directory (unless u-boot binary)
- copy a Makefile to the build-directory

There is just one file missing: the u-boot binary (`u-boot.bin`) one would like to
embed into the bootcontainer to create. Just copy it to your bootcontainer
build environment:
```
$ cp -avr PATH/TO/YOUR/u-boot.bin PATH/TO/YOUR/COPY/OF/cgtbcbe/BUILD_SX8X_2GB/.
```

**INFO:** 
When setting up a bootcontainer build environment, it's possbile to pass the
desired u-boot binary (`u-boot.bin`) via `-u PATH/TO/YOUR/u-boot.bin` directly to
`mkbcbe.sh`.
In that case, `mkbcbe.sh` copies the u-boot binary to the bootcontainer build
environment automatically. The help printed above shows all the convenience 
options.

### How to Build a Bootcontainer

In order to create/build a bootcontainer, activate the previously created
bootcontainer build environment by sourcing `mkbcbe.sh` as follows:
```
$ cd cgtbcbe
$ source ./mkbcbe.sh BUILD_SX8X_2GB

Command to create a boot container...
  ..to boot from SPI-flash:
      rm -f u-boot-hash.bin; make flash_flexspi
  ..to boot from SD-card:
      rm -f u-boot-hash.bin; make flash

ATTENTION:
- always use 'rm -f u-boot-hash.bin' instead of 'make clean' !
- 'make clean' usually corruptes the build directory !
```

For instance, to build a sd bootcontainer, just type:
```
$ rm -f u-boot-hash.bin; make flash

./mkimage_imx8 -commit > head.hash
588+1 records in
588+1 records out
602479 bytes (602 kB) copied, 0.00201963 s, 298 MB/s
./mkimage_imx8 -soc QX -rev B0 -dev flexspi -append mx8qx-ahab-container.img -c -scfw scfw_tcm.bin -ap u-boot-atf.bin a35 0x80000000 -out flash.bin
SOC: QX 
REVISION: B0 
BOOT DEVICE:    flexspi
New Container:  0
SCFW:   scfw_tcm.bin
AP:     u-boot-atf.bin  core: a35 addr: 0x80000000
Output: flash.bin
# [...]
```

A bootcontainer called `flash.bin` has been created - have fun!

### Overview: Components of the Local cgtbcbe Clone

cgtbcbe mainly consists of a central script (`mkbcbe.sh`), a distribtution of
required files (`mx8x/pk_rel, mx8mp/pk_rel `) and config files (`mx8x/config, mx8mp/config`)
defining how a bootcontainer is built:
```
mkbcbe.sh         # script/link to setup a bootcontainer build environment
mx8mp
mx8mp/config      # config files used to build a specifc i.MX8MP bootcontainer
mx8mp/pk_rel      # binaries, scripts and executables required to build a bootcontainer for i.MX8MP-based products
mx8x
mx8x/config       # config files used to build a specifc i.MX8X bootcontainer
mx8x/pk_rel       # binaries, scripts and executables required to build a bootcontainer for i.MX8X-based products
script/mkbcbe.sh  # don't call the script directly! Always use the link at the top-level directory.
README.md         # the document, you are currently reading
```
