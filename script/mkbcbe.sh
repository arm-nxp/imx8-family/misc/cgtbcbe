#!/bin/bash
################################################################################
# Copyright (C) 2019 congatec AG, Alexander Pockes
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
################################################################################ 

# --
# declaring arrays used at config files
# ---
declare -A pk_rel_link_src
declare -A pk_rel_link_target
declare -A pk_rel_vars
declare -A pk_rel_build_target
declare -A arg_bin

# --
# constants
# ---
# $0 is not usable due to the nature of >the magic<,
# so script-name has to be stored in $VAR0
VAR0="./mkbcbe.sh"
BE_LOCAL_CONFIG=".beconfig.inc.sh"

# --
# variables
# ---
arg_build_dir=""
arg_setup_config_file=""
flag_archive_mode="1"
retval="0"


#--
# exit invocation if git-lfs isn't installed
#---
if [[ $(git lfs install 2>&1) != *"Git LFS initialized."* ]]
then
    echo -e "\nERROR: please install git lfs first; create a clean clone of cgtbcbe afterwards, exiting..."
    exit 8
fi


# --
# the magic
# ---
if [ "$0" = "${VAR0}" ]
then
# case: called - build directory setup
    usage()
    {
        echo -e
        echo -e "usage:"
        echo -e "  ${VAR0} -b <BUILD_DIR> -c <CONFIG_TO_USE> [-u UBOOT_BINARY]"
        echo -e
        echo -e "  required switches:"
        echo -e "    -b \t build directory (environment) to be created by mkbcbe.sh"
        echo -e "    -c \t config file to use"
        echo -e "  optional switches, (re)defining binaries to be copied to the build env:"
        echo -e "    -u \t u-boot binary"
        echo -e "    -s \t redefine SCFW binary"
        echo -e "    -m \t m40 firmware binary"
        echo -e "    -n \t m41 firmware binary"
        echo -e "    -t \t don't copy all required files to build env; just create links"
        echo -e
    }

    # process the commandline switches
    while getopts "b:c:u:s:m:n:t" opt
    do
        case "$opt" in
            b)
                arg_build_dir="$OPTARG"
                ;;
            c)
                if [ ! -f "$OPTARG" ]
                then
                    echo -e "\nERROR: > $OPTARG < config file not found, exiting..."
                    exit 9
                fi
                arg_setup_config_file="$OPTARG"
                ;;
            u)
                if [ ! -f "$OPTARG" ]
                then
                    echo -e "\nERROR: > $OPTARG < is not a file, exiting..."
                    exit 9
                fi
                arg_bin[UBOOT_BIN]="$OPTARG"
                ;;
            s)
                if [ ! -f "$OPTARG" ]
                then
                    echo -e "\nERROR: > $OPTARG is < not a file, exiting..."
                    exit 9
                fi
                arg_bin[SCFW_BIN]="$OPTARG"
                ;;
            m)
                if [ ! -f "$OPTARG" ]
                then
                    echo -e "\nERROR: > $OPTARG < is not a file, exiting..."
                    exit 9
                fi
                arg_bin[M40_BIN]="$OPTARG"
                ;;
            n)
                if [ ! -f "$OPTARG" ]
                then
                    echo -e "\nERROR: > $OPTARG < is not a file, exiting..."
                    exit 9
                fi
                arg_bin[M41_BIN]="$OPTARG"
                ;;
            t)
                flag_archive_mode="0"
                ;;
            *)
                usage
                exit 1
                ;;
        esac
    done

    # check if required switches are passed
    if [ -z "$arg_build_dir" ]
    then
        usage
        exit 1
    fi

    if [ -z "$arg_setup_config_file" ]
    then
        usage
        exit 1
    fi

    # exit script if build dir already exists...
    if [ -d "${arg_build_dir}" ]
    then
        echo
        echo "WARNING: build directory > ${arg_build_dir} < already exists."
        echo "In order to activate the build environment, run:"
        echo "source ${VAR0} ${arg_build_dir}"
        echo
        exit 2
    fi

    # load variable definitions defined at config file set by -c <CONFIG_FILE>
    source ${arg_setup_config_file}

    # create build dir directory set by required switch -b <BUILD_DIR>
    if ! mkdir ${arg_build_dir}
    then
        echo -e "\nERROR: can't create build directory, exiting..."
        exit 3
    fi
    cd "${arg_build_dir}"

    # determine path of directory where the pk_rel binaries reside
    BIN_DIR=$(dirname $(realpath --relative-to $PWD "$BIN_DIR"))

    # add BIN_DIR to the filenames defined at config file
    for key in ${!pk_rel_link_src[@]};
    do
        if [ ! -z "${pk_rel_link_src[$key]}" ]
        then
            pk_rel_link_src[$key]="${BIN_DIR}/${pk_rel_link_src[$key]}"
        fi
    done

    # reset path to single binaries if respective cmdl switches have been set
    for key in ${!arg_bin[@]};
    do
        if [ ! -z "${arg_bin[$key]}" ]
        then
            pk_rel_link_src[$key]=$(readlink -f ${arg_bin[$key]})
        fi
    done

    # populate build dir with links/duplicates of the required/defined files
    for key in ${!pk_rel_link_src[@]};
    do
        retval="0"
        if [ ! -z "${pk_rel_link_src[$key]}" ]
        then
            if [ $flag_archive_mode -eq 0 ]
            then
                ln -s "${pk_rel_link_src[$key]}" "${pk_rel_link_target[$key]}"
            else
                cp -avrL "${pk_rel_link_src[$key]}" $(basename "${pk_rel_link_src[$key]}") > /dev/null
                if [ "$(basename "${pk_rel_link_src[$key]}")" != "${pk_rel_link_target[$key]}" ]
                then
                    ln -s $(basename "${pk_rel_link_src[$key]}") "${pk_rel_link_target[$key]}"
                fi
            fi
            retval=$?
            if [ $retval -ne 0 ]
            then
                echo -e "\nERROR: couldn't populate build directory, exiting"
                exit 3
            fi

            # debug
            # echo "${key}:"
            # echo "  ${pk_rel_link_src[$key]}"
        fi
    done

    # copy makefile into created build directory
    if ! cp -avrL "${BIN_DIR}/${MAKEFILE_NAME}" ./Makefile > /dev/null
    then
        echo -e "\nERROR: can't copy required files to build directory, exiting..."
        exit 3
    fi

    # last step: every build dir holds a local config indicating a valid BE;
    # also used to redefine makefile variables using shell env variables
    if ! touch "$BE_LOCAL_CONFIG"
    then
        echo -e "\nERROR: can't create local BE config at build directory, exiting..."
        exit 3
    fi
    echo -e "# $(basename $arg_setup_config_file)\n" >> $BE_LOCAL_CONFIG
    for key in ${!pk_rel_vars[@]};
    do
        echo "export ${key}=\"${pk_rel_vars[$key]}\"" >> $BE_LOCAL_CONFIG
    done
    
    for key in ${!pk_rel_build_target[@]};
    do
        echo "pk_rel_build_target[$key]=\"${pk_rel_build_target[$key]}\"" >> $BE_LOCAL_CONFIG
    done

    # ensure that makefile variable redefinitions via shell env get precedence
    echo "alias make='make -e'" >> $BE_LOCAL_CONFIG

    echo
    echo "PASS: build container build environment created at $arg_build_dir"
    echo "In order to activate it, run:"
    echo "source ${VAR0} ${arg_build_dir}"
    echo

    exit 0
else
    # case: sourced - build directory activation
    if [ $# -ne 1 ]
    then
        echo
        echo "usage:"
        echo "  source ${VAR0} <BUILD_DIR_TO_ACTIVATE>"
        echo
    else
        arg_build_dir="$1"
        if [ -r ${arg_build_dir}/${BE_LOCAL_CONFIG} ]
        then
            cd "${arg_build_dir}"
            source "$BE_LOCAL_CONFIG"

            # print supported build targets
            echo
            echo "Command to create a boot container..."
            for key in ${!pk_rel_build_target[@]};
            do
                echo "  ..to boot from $key:"
                echo "      rm -f u-boot-hash.bin; make ${pk_rel_build_target[$key]}"
            done
            echo
            echo "ATTENTION:"
            echo "- always use 'rm -f u-boot-hash.bin' instead of 'make clean' !" 
            echo "- 'make clean' usually corruptes the build directory !"
            echo
        else
            echo -e "\nERROR: build environment does not exist or is corrupted, exiting..."
        fi
    fi
fi
